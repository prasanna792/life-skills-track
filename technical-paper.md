# Basic Data Types and Data Structures in JavaScript

* JavaScript is a programming language that supports various data types and data structures. In this document, mentioning some of the fundamental data types and data structures in JavaScript,along with code samples for each.

## Data Types

### 1. **Number**

The `number` data type represents numeric values in JavaScript. It includes integers,decimals and special numeric values like `Infinity` and `NaN`.

```
let age = 25;
let temperature = 98.6;
let infinityValue = Infinity;
let notANumber = NaN;
```

### 2. **String**

The `string` data type represents textual data and is enclosed in single or double quotation marks.

```
let name = "John Doe";
let message = "Hello, world!";
```

### 3. **Boolean**

The boolean data type represents logical values, `true` or `false`.

```
let isWorking = true;
let isLoggedIn = false;
```

### 4. **Null** 

The `null` data type represents the absence of any value

```
let data = null;
```

### 5. **Undefined**

The `undefined` data type represents a value that has been declared but has not been assigned a value.

```
let variable;
console.log(variable);
```

## Data Structures

### 1. Array

An `array` is an ordered collection of values enclosed in square brackets. It can store multiple values of any data type.

```
let numbers = [1,2,3,4,5];
let fruits = ["apple", "banana", "orange"];
```

### 2. Object 

An `object` is an unordered collection of key-value pairs,where each key is a unique string and each value can be of any data type.

```
let person = {
    name: "John",
    age: 25,
    isStuddent: true
};
```

### 3. Map

A `Map` is a collection of key-value pairs where the keys can be of any data type. It provides an ordered list of values based on the insertion order.

```
let map = new Map();
map.set("name","John");
map.set("age",25);
```

### 4. Set

A `Set` is a collection of unique values where any value,whether primitive or object references,can occur only once.

```
let set = new Set();
set.add(1);
set.add(2);
set.add(3);
```

### 5. Stack(using Array)

A `stack` is a data structure that follows the Last-In-First-Out(LIFO) principle. It can be implemented using an array.

```
let stack = [];
stack.push(1);
stack.push(2);
stack.push(3);
stack.pop();  // output: 3
```

### 6.Queue(using Array)

A `queue` is a data structure that follows the First-In-First-Out(FIFO) principle. It can be implemented using an array.

```
let queue = [];
queue.push(1);
queue.push(2);
queue.push(3);
queue.shift();   // output: 1
```


#### References

* [freeCodeCamp article](https://www.freecodecamp.org/news/data-structures-in-javascript-with-examples/)
* [mdn web docs](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures)
* [geeksforgeeks article](https://www.geeksforgeeks.org/learn-data-structures-with-javascript-dsa-tutorial/)
